Page({
    current: "1",
    onChange(event) {
        const { value } = event.detail;
        this.setData({ current: value });
    }
});
