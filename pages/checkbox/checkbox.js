// pages/checkbox/checkbox.js
Page({
    /**
     * 页面的初始数据
     */
    data: {
        current: []
    },
    onChange(event) {
        const { value } = event.detail;
        this.setData({ current: value });
    }
});
