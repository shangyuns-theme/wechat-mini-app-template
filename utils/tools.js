export function cssVar(name, obj) {
    const variable = [];
    if (obj) {
        Object.keys(obj).forEach((key) => {
            if (obj[key]) {
                variable.push(`--${name}-${key}: ${obj[key]}`);
            }
        });
    }
    return variable.join(";");
}



export function clnm(...args) {
    if (args.length) {
      return Array.from(arguments).filter((item) => item).join(" ");
    }
    return "";
  }