
function getRelations(relation, path) {
    return Behavior({
        created() {
            Object.defineProperty(this, `$${relation}`, {
                get: () => {
                    const nodes = this.getRelationNodes(path) || [];
                    return relation === "parent" ? nodes[0] : nodes;
                }
            });
        }
    });
}
export function customComponent(options) {
    const { relations , behaviors = []} = options;
    if (relations) {
        const map = {};
        Object.keys(relations).forEach((path) => {
            const comp = relations[path];
            const relation = ["parent", "ancestor"].includes(comp.type) ? "parent" : "children";
            const mixin = getRelations(relation, path);
            map[relation] = mixin;
        });
        behaviors.push(...Object.keys(map).map((key) => map[key]));
    }

    options.behaviors = [...behaviors];
    return options
}
