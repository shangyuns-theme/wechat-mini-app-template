
//wxe2e74b866c0bb533 wx89f08b0d08665cab
const isRelease = wx.getAccountInfoSync().miniProgram.appId === 'wx75adc599d034dbe2';

export const  VERSION = wx.getAccountInfoSync().miniProgram.version;
// HTTP地址
export const HTTP = isRelease?"https://proj-betta-patient-manage.xingshulin.com":"https://qa-proj-betta-patient-manage.xingshulin.com";

//TIMSDKAppID
export const TIM_SDKAPPID = isRelease ? '1400609679':'1400546184';

//七牛云上传地址
export const QINIUYUN = 'https://upload-z1.qiniup.com';

export const  SENSORSURL = isRelease ? 'https://sd.xingshulin.com:8446/sa':'https://sd.xingshulin.com:8446/sa?project=QA';
export const  SENSORSENV = isRelease ? 'prod':'qa';
