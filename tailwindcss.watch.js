const postcss = require("postcss");
var autoprefixer = require("autoprefixer");
var postcssNested = require("postcss-nested");
var tailwindcss = require("tailwindcss");
var cssnano = require("cssnano");
var pxtransform = require("postcss-pxtransform");
var remToRpx = require("postcss-rem-to-responsive-pixel");
var chokidar = require("chokidar");
const tailwindConfig = require("./tailwind.config");
const fs = require("fs");

function scssToCss() {
    postcss([
        tailwindcss(tailwindConfig),
        remToRpx({
            rootValue: 32,
            propList: ["*"],
            transformUnit: "rpx"
        }),
        pxtransform({ platform: "weapp", designWidth: 750 }),
    ])
        .process("@tailwind utilities;", { from: undefined })
        .then((result) => {
            var css = result.css;
            fs.writeFile("./app.less", css.replace(/\\\[(.*?)\\\]/g, "_$1_").replace(/\n}/g, ';\n}'), () => true);
        });
}
chokidar
    .watch([`./**/*.wxml`], {
        ignoreInitial: true
    })
    .on("change", (watchPath, stats) => {
        console.log("change");
        scssToCss();
    })
    .on("add", (watchPath, stats) => {
        console.log("add");
    });
