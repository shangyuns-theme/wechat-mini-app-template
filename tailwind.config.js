module.exports = {
    content: {
        files: ["./**/*.wxml","./**/*.less", "./components/**/*.wxml"],
        transform: (content) => {
            // 编译_$1_ 替换[]

            const classRegex = /class="([^"]+)"/g;

            // 对匹配到的 class 字符串中的开始和结束下划线进行替换
            return content.replace(classRegex, (match, p1) => {
                // 替换 class 属性内的开始和结束下划线
                let updatedClass = p1.replace(/_(.+?)_/g, "[$1]");
                return `class="${updatedClass}"`;
            });
        }
    },
    theme: {
        extend: {
            colors: {
                primary:{
                    DEFAULT: "#00D778"
                },
                secondary:{
                    DEFAULT: "#00D768"
                },
                success:{
                    DEFAULT: "#00D758"
                },
                warning:{
                    DEFAULT: "#00D748"
                },
                danger:{
                    DEFAULT: "#00D738"
                }
            }
        }
    },
    plugins: []
};
