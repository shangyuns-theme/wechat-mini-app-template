import { cssVar, clnm } from "@utils/tools.js";
import { properties } from "./props";
import { customComponent } from "@utils/customComponent.js";
const option = customComponent({
    data: {},
    relations: {
        "../checkbox-group/checkbox-group": {
            type: "ancestor"
        }
    },
    properties: properties,
    methods: {
        doChange(detail) {
            if (this.$parent) {
                this.$parent.updateValue(detail);
            } else {
                this.triggerEvent("change", detail);
            }
        },
        onChange() {
            const { checked, value } = this.data;
            this.doChange({ checked: !checked, value });
        }
    }
});
Component(option);
