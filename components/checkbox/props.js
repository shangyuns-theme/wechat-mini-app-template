export const properties = {
    label: {
        type: String,
        value: "",
        description: "名称",
        schema: {
            type: "input"
        }
    },
    size: {
        type: String,
        value: "md",
        description: "单选框大小",
        schema: {
            type: "select",
            option: ["sm", "md", "lg"]
        }
    },
    checked: {
        type: Boolean,
        value: false,
        description: "是否选中",
        schema: {
            type: "switch"
        }
    },
    checkedColor: {
        type: String,
        value: "#000",
        description: "选中颜色",
        schema: {
            type: "color"
        }
    },
    notCheckedColor: {
        type: String,
        value: "#999",
        description: "未选中颜色",
        schema: {
            type: "color"
        }
    },
    value: {
        type: String,
        value: "",
        description: "单选按钮的值",
        schema: {
            type: "input"
        }
    }
};
