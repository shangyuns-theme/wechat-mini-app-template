export const properties = {
    size: {
        type: String,
        value: "md",
        description: "按钮大小",
        schema: {
            type: "select",
            option: ["sm", "md", "lg"]
        }
    },
    isOutlined: {
        type: Boolean,
        value: false,
        description: "按钮是否漏空",
        schema: {
            type: "switch"
        }
    },
    rounded: {
        type: String,
        value: "6",
        description: "按钮圆角",
        schema: {
            type: "input"
        }
    },
    color: {
        type: String,
        value: "#fff",
        description: "字体颜色",
        schema: {
            type: "color"
        }
    },
    bgColor: {
        type: String,
        value: "#000",
        description: "背景颜色",
        schema: {
            type: "color"
        }
    },
    block: {
        type: Boolean,
        value: true,
        description: "是否为块级元素",
        schema: {
            type: "switch"
        }
    },
    load: {
        type: Boolean,
        value: false,
        description: "是否显示加载中状态",
        schema: {
            type: "switch"
        }
    }
};
