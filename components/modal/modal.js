Component({
    data: {
        show: false
    },
    options: {
        addGlobalClass: true
    },
    properties: {
        visible: {
            type: Boolean,
            value: false
        }
    },
    methods: {
        toggleVisible() {
            this.setData({
                show: !this.data.show
            });
        },
        handleCancel() {
            this.toggleVisible();
            this.triggerEvent("cancel", {}, {});
        }
    },
    observers: {
        // visible: function () {
        //   this.changeShowModel();
        // }
    }
});
