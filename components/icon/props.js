export const properties = {
    width: {
        type: String,
        value: "16px",
        description: "图标宽度",
        schema: {
            type: "input"
        }
    },
    height: {
        type: String,
        value: "16px",
        description: "图标高度",
        schema: {
            type: "input"
        }
    },
    color: {
        type: String,
        value: "rgb(255,255,255)",
        description: "图标颜色",
        schema: {
            type: "color"
        }
    },
    name: {
        type: String,
        value: "",
        description: "图标名称",
        schema: {
            type: "input"
        }
    }
};
