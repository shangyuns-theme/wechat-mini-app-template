import { properties } from "./props";

Component({
  data: {
    iconColor: ""
  },
  properties:properties,
  observers: {
    "color": function (value) {
      this.setData({
        iconColor: encodeURIComponent(value)
      });
    }
  },
  methods: {}
})