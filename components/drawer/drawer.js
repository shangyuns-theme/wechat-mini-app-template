// components/drawer/drawer.ts
Component({
    options: {
        addGlobalClass: true,
        multipleSlots: true
    },
    /**
     * 组件的属性列表
     */
    properties: {
        title: {
            type: String,
            value: "Title"
        },
        height: {
            type: String,
            value: "80vh"
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        show: false,
        drawerStatus: ""
    },
    observers: {
        visible: function (value) {
            if (value) {
                this.setData({
                    show: true
                });
                wx.nextTick(() => {
                    this.setData({
                        drawerStatus: "drawer-show"
                    });
                });
            }
            if (this.data.show && !value) {
                this.handleHide();
            }
        }
    },
    /**
     * 组件的方法列表
     */
    methods: {
        toggleVisible() {
            !this.data.show ? this.show() : this.hide();
        },
        show() {
            this.setData({
                show: true
            });
            wx.nextTick(() => {
                this.setData({
                    drawerStatus: "drawer-show"
                });
            });
        },
        hide() {
            this.setData({
                drawerStatus: "drawer-hide"
            });
            setTimeout(() => {
                this.setData({
                    show: false
                });
            }, 200);
        }
    }
});
