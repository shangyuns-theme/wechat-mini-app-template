import { cssVar, clnm } from "@utils/tools.js";
import {properties} from './props'
Component({
    options: {
        addGlobalClass: true
    },
    /**
     * 组件的属性列表
     */
    properties: properties,

    /**
     * 组件的初始数据
     */
    data: {
        styleStr: ""
    },
    lifetimes: {
        attached: function () {
            this.systemInfo();
        }
    },
    observers: {
        'bgColor, color': function () {
            this.systemInfo();
        }
    },
    /**
     * 组件的方法列表
     */
    methods: {
        systemInfo() {
            let rect = null;
            if (wx.getMenuButtonBoundingClientRect) {
                rect = wx.getMenuButtonBoundingClientRect();
            }
            const { bgColor, color,position } = this.data;
            let right = 100;
            wx.getSystemInfo({
                success: (res) => {
                    const { statusBarHeight } =  wx.getSystemInfoSync();
                    if (rect && (res === null || res === void 0 ? void 0 : res.windowWidth)) {
                        right = res.windowWidth - rect.left;
                    }
                    this.setData({
                        styleStr: cssVar("navbar", { paddingTop: `${position != 'fixed'?0:statusBarHeight}px`, right: `${right}px`, color, bgColor,position })
                    });
                },
                fail: (err) => {
                    console.error("navbar 获取系统信息失败", err);
                }
            });
        },
        onBack() {
            wx.navigateBack({
                delta: 1
            });
        }
    }
});
