export const properties = {
    navigatorType: {
        type: String,
        value: "default",
        description: "default 导航栏  custom 自定义导航栏",
        schema: {
            type: "select",
            option: ["default", "custom"]
        }
    },
    isBack: {
        type: Boolean,
        value: true,
        description: "是否显示返回按钮",
        schema: {
            type: "switch"
        }
    },
    position: {
        type: String,
        value: "fixed",
        description: "导航栏定位模式 fixed 固定头部  relative 相对定位  absolute 绝对定位",
        schema: {
            type: "select",
            option: ["fixed", "relative", "absolute"]
        }
    },
    bgColor: {
        type: String,
        value: "#ffffff",
        description: "背景颜色",
        schema: {
            type: "color"
        }
    },
    color: {
        type: String,
        value: "#000000",
        description: "字体颜色",
        schema: {
            type: "color"
        }
    },
    title: {
        type: String,
        value: "导航栏",
        description: "标题 navigatorType 为 default 时有效，custom 时无效",
        schema: {
            type: "input"
        }
    }
};
