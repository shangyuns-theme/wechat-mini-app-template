Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isChecked: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {
    toggleSwitch() {
      const checked =!this.data.isChecked;
      this.setData({
        isChecked: checked
      });
      // 触发自定义事件，将开关状态传递出去
      this.triggerEvent('change', { value: checked });
    }
  }
})