import { customComponent } from "@utils/customComponent.js";
import { properties } from "./props";

const option = customComponent({
    data: {},
    relations: {
        "../radio/radio": {
            type: "descendant",
            linked: function (target) {
                const { value } = this.data;
                target.setData({
                    checked: value === target.data.value
                });
            }
        }
    },
    observers: {
        value(v) {
            this.$children.forEach((item) => {
                item.setData({
                    checked: v === item.data.value
                });
            });
        }
    },
    properties: properties,
    methods: {
       
        updateValue(value) {
            this.triggerEvent("change", { value });
        }
    }
});
Component(option);
