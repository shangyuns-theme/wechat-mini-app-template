import { properties } from "./props";
import { customComponent } from "@utils/customComponent.js";
const option = customComponent({
    data: {},
    relations: {
        "../radio-group/radio-group": {
            type: "ancestor"
        }
    },
    properties: properties,
    methods: {
        onChange() {
            const { checked, value } = this.data;
            if (this.$parent) {
                this.$parent.updateValue(!checked ? value : null);
            } else {
                this.triggerEvent("change", { checked: !checked });
            }
        }
    },
    ready() {}
});
Component(option);
