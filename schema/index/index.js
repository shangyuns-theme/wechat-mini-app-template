Component({
    data: {
        list: []
    },
    properties: {
        config: {
            type: Object
        }
    },
    observers: {
        config: function () {
            this.setConfigList();
        }
    },
    created() {
        this.setConfigList();
    },
    methods: {
        setConfigList() {
            const { config } = this.data;
            if (config) {
                const list = Object.keys(config).map((key) => {
                    return {
                        ...config[key],
                        key
                    };
                });
                this.setData({
                    list
                });
            }
        }
    }
});
